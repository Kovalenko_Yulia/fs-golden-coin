//all dropdown menus have opacity: 0; slide up all menus and change opacity to 1 after sliding up (1sec)
$(window).on('load',function(){
    $('.first-screen .myContainer .right .bottom .user-img .dropdownMenu').slideUp();
    $('.first-screen .myContainer .languages .notActive').slideUp();
    $('.pseudoSelect').each(function(){
        $(this).next('ul').slideUp();
    });
    setTimeout(function () {
        $('.first-screen .myContainer .languages .notActive').css('opacity','1');
        $('.first-screen .myContainer .right .bottom .user-img .dropdownMenu').css('opacity','1');
        $('.pseudoSelect').each(function(){
            $(this).next('ul').css('opacity','1');
        });
    }, 1000);

    $('.preloader').addClass('done');
});



$(document).ready(function(){
    //initialize slider
    $('.first-screen .slider').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        fade: true,
        speed: 3000,
        dots: true
    });

    new WOW().init();

    // INITIALIZE ANIMSITION
    $(".animsition").animsition({
        inClass: 'fade-in-up-sm',
        outClass: 'fade-out-up-sm',
        inDuration: 800,
        outDuration: 800,
        linkElement: '.animsition-link',
        // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
        loading: false,
        loadingParentElement: 'body', //animsition wrapper element
        loadingClass: 'animsition-loading',
        loadingInner: '', // e.g '<img src="loading.svg" />'
        timeout: true,
        timeoutCountdown: 800,
        onLoadEvent: false,
        browser: [ 'animation-duration', '-webkit-animation-duration'],
        // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
        // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
        overlay : false,
        overlayClass : 'animsition-overlay-slide',
        overlayParentElement : 'body',
        transition: function(url){ window.location.href = url; }
    });

    setTimeout(function () {
        $('.providers').show().addClass('wow animated fadeInUp');}, 1500
    );


    //hide/show registered user menu
    $(document).on('click', '.first-screen .myContainer .right .bottom .user-img img', function(){
    	$('.first-screen .myContainer .right .bottom .user-img .dropdownMenu').slideToggle(250);
    });

    //hide/show languages
    $(document).on('click', '.first-screen .myContainer .languages .active', function(){
        $('.first-screen .myContainer .languages .notActive').slideToggle(150);
    });

    //change language
    $(document).on('click', '.first-screen .myContainer .languages .notActive', function(){
        $(this).addClass('active').removeClass('notActive');
        $(this).siblings('p').addClass('notActive').removeClass('active').fadeOut(150).slideUp();
    });

    //toggle pseudoSelect list
    $(document).on('click', '.pseudoSelect', function(){
        $(this).toggleClass('active').next('ul').slideToggle(400);
    });

    //choose pseudoSelect item and sort the list
    $(document).on('click', '.pseudoSelect + ul li', function(){
        let value = $(this).text();
        let className = $(this).attr('class');
        $(this).parents('ul').slideUp(400);
        $(this).parents('ul').siblings('.pseudoSelect').removeClass('active').html(value);
        $(this).parents('ul').siblings('input.hidden').val(value);
        $(this).parents('.filters').next('.content').find('.game').each(function(){
            if ($(this).hasClass(className)){
                if(!$(this).is(":visible")){
                    $(this).fadeIn(400);
                }
            } else{
                $(this).fadeOut(0);
            }
        });
    });

    //search provider and sort the list
    $(document).on('input', '.filters .search input', function(){
        let value = $(this).val().toLowerCase();
        $(this).parents('.filters').next('.content').find('.game').each(function(){
            let description = $(this).find('.text p').text().toLowerCase();
            if (description.indexOf(value) != -1){
                if(!$(this).is(":visible")){
                    $(this).fadeIn(400);
                }
            } else {
                $(this).fadeOut(0);
            }
        });
    });

    //sort games by menu click
    $(document).on('click', '.filters .menu a', function(e){
        e.preventDefault();
        let className = $(this).attr('class');
        $(this).parents('.filters').next('.content').find('.game').each(function(){
            if ($(this).hasClass(className)){
                if(!$(this).is(":visible")){
                    $(this).fadeIn(400);
                }
            } else{
                $(this).fadeOut(0);
            }
        });
    });

    //change winners randomly (with interval from 5 to 20 seconds)
    (function(){
        let min = 1,
            max = 20;
        let random = Math.floor(Math.random() * (max - min + 1)) + min;

        let winnerItem = $('.jackpots .right ul li').first().html();
        $('.jackpots .right ul li').first().addClass('old');
        setTimeout(function () {
            $('.jackpots .right ul li').first().remove();
            $('.jackpots .right ul li').last().after('<li>' + winnerItem + '</li>');
        }, 1000);
        setTimeout(arguments.callee, random*1000);
    })();

    //show swing-guy animation when block is visible (scrolled to)
    $('.jackpots').removeClass('visible');
    if($('.jackpots .left').css('visibility') != 'hidden'){
        $('.jackpots').addClass('visible');
    }
    $(window).on('scroll', function() {
        if($('.jackpots .left').css('animation-name') != 'none'){
            $('.jackpots').addClass('visible');
        }
    });

    // Add smooth scrolling to all links
    $("a").on('click', function(event) {
        event.preventDefault();
        if (this.hash !== "") {

            var hash = this.hash;

            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function(){
                window.location.hash = hash;
            });
        }
    });

    //hide/show tabs content
    $(function () {
        $('.tab-item').not(':first').hide();
        $('.tab').click(function () {
            $('.tab').removeClass('tab-active').eq($(this).index()).addClass('tab-active');
            $('.tab-item').hide().eq($(this).index()).fadeIn(500);
        }).eq(0).addClass('tab-active');
    });

    $(".picker input").on("change", function() {
        var date = $(this).val();
        $(this).parents('.picker').siblings('input').val(date);
    });

    //to top button
    $(function() {
        $(window).scroll(function() {
            if($(this).scrollTop() >= 50) {
                $('#toTop').addClass('visible');
            } else {
            $('#toTop').removeClass('visible');
            }
        });
            $('#toTop').click(function() {
            $('body,html').animate({scrollTop:0},800);
        });
    });

})

